'''
Created on 2 oct. 2020

@author: Denise
'''
import datetime

class Alumno:
    def __init__(self, nombre, edad, carrera, fechaInsc):
        if nombre is None:
            self.__nombre = str("")
        else:
            self.__nombre = nombre
        
        if edad is None:
            self.__edad = int()
        else:
            self.__edad = edad
        
        if carrera is None:
            self.__carrera = str("")
        else:
            self.__carrera = carrera
        
        if fechaInsc is None:
            self.__fechaInsc = datetime.date.today()
        else:
            self.__fechaInsc = fechaInsc
    
    def getNombre(self):
        return self.__nombre
    def setNombre(self, nombre):
        self.__nombre = nombre
        
    def getEdad(self):
        return self.__edad
    def setEdad(self, edad):
        self.__edad = edad
        
    def getCarrera(self):
        return self.__carrera
    def setCarrera(self, carrera):
        self.__carrera = carrera
        
    def getFechaInsc(self):
        return self.__fechaInsc
    def setFechaInsc(self, fechaInsc):
        self.__fechaInsc = fechaInsc
        
    def __str__(self):
        fecha = datetime.date.today()
        fecha = self.getFechaInsc()
        return("Nombre: " + self.getNombre() + ", Edad:" + str(self.getEdad()) + ", Carrera: " + self.getCarrera()+", Fecha de Inscripcion: " + str(fecha.day) + "/" + str(fecha.month) + "/" + str(fecha.year))
        
        
class ColeccionAlumnos:
    def __init__(self, mapAlumnos):
        if mapAlumnos is None:
            self.__mapAlumnos = dict()
        else:
            self.__mapAlumnos = mapAlumnos
            
    def getMapAlumnos(self):
        return self.__mapAlumnos
    def setMapAlumnos(self, mapAlumnos):
        self.__mapAlumnos = mapAlumnos
        
    def validacionNum(self):
        error = 1
        while error == 1:
            try:
                r = int(input())
            except:
                print("Ingrese uncamente numeros, intentelo de nuevo: ")
                error = 1
            else:
                if r > 0:
                    error = 0
                else: 
                    print("Solo numeros, intentelo de nuevo: ")
                    error = 1
        return r
    
    def validacionCarrera(self):
        error = 1
        carrera = ""
        i = 0
        
        while(error == 1):
            if(i == 0):
                print("Carrera:")
            else:
                carrera = input().lower()
            i += 1
            if(carrera == "isc" or carrera == "iia" or carrera == "im" or carrera == "la" or carrera == "cp"):
                error = 0
            else:
                if(i != 1):
                    print("No existe la carrera ingresada, ingresela de nuevo!")
                error = 1
        return carrera
    
    def validacionFech(self):
        fech = []
        error = 1
        while(error == 1):
            error = 0
            print("Fecha(dd/mm/aaaa): ")
            entrada = str(input())
            try:
                fech.append(int(entrada[6:10]))
                fech.append(int(entrada[3:5]))
                fech.append(int(entrada[0:2]))
            except:
                print("Fecha incorrecta, intente de nuevo!")
                error = 1
                
        fecha = datetime.date(fech[0], fech[1], fech[2])
        return fecha
    
    def llenarList(self, cantidad):
        mapAlumnos = dict()
        
        for i in range(cantidad):
            print("Alumno " + str(i + 1))
            nombre = str(input("Ingrese el nombre: "))
            print("Ingrese la edad: ")
            edad = self.validacionNum()
            carrera = self.validacionCarrera()
            fechaInsc = self.validacionFech()
            a = Alumno(nombre, edad, carrera, fechaInsc)
            print(a)
            mapAlumnos[i] = a
        self.setMapAlumnos(mapAlumnos)
        
    def vaciarList(self):
        vaciar = dict()
        self.setMapAlumnos(vaciar)
        print("La lista ha sido vaciada!")
    
    def mostrarAlumnosporCarrera(self, carrera):
        print("Alumnos por carrera: " + carrera)
        mapAlumnos = self.getMapAlumnos()
        for i in range(len(mapAlumnos)):
            alTot = mapAlumnos[i]
            if(alTot.getCarrera() == carrera):
                print(alTot)
                
    def calcularPromEdades(self):
        mapAlumnos = self.getMapAlumnos()
        prom = 0.0
        for i in range(len(mapAlumnos)):
            alTot = mapAlumnos[i]
            prom = prom + alTot.getEdad()
        prom = prom / len(mapAlumnos)
        return prom
    
    def filtroFech(self, fecha):
        print("Alumnos inscritos despues de la fecha indicada (10/08/2016): ")
        mapAlumnos = self.getMapAlumnos()
        for i in range (len(mapAlumnos)):
            alTot = mapAlumnos[i]
            fech = datetime.date.today()
            fech = fecha
            if(alTot.getFechaInsc() > fech):
                print(alTot)
                
                
a1 = dict()
ca = ColeccionAlumnos(a1)
fecha = datetime.date(2016, 8, 10)

opcion = 0
opcion2 = 0

while(opcion != 6):
    print("1- Llenar la lista")
    print("2- Vaciar la lista")
    print("3- Mostrar alumnos por carrera")
    print("4- Calcular el promedio de las edades")
    print("5- Mostrar a los alumnos inscritos despues de la fecha indicada(10/08/2016)")
    print("6- Salir del menu")
    
    opcion = ca.validacionNum()
    if opcion == 1:
        ca.llenarList(5)
    elif opcion == 2:
        ca.vaciarList()
    elif opcion == 3:
        carrera = ""
        opcion2 = 7 
        while(opcion2 != 6):
            print("1- ISC")
            print("2- IIA")
            print("3- IM")
            print("4- LA")
            print("5- CP")
            opcion2 = ca.validacionNum()
        
            if(opcion2 == 1):
                carrera = "isc"
                opcion2 = 6
            elif(opcion2==2):
                carrera="iia"
                opcion2=6
            elif(opcion2==3):
                carrera="im"
                opcion2=6
            elif(opcion2==4):                
                carrera = "ia"
                opcion2 = 6
            elif(opcion2==5):
                carrera="cp"
                opcion2=6            
            else:
                print("Esa opcion no esta en el menu!")
                opcion2=7
                  
        ca.mostrarAlumnosporCarrera(carrera)
         
    elif opcion == 4:
        print("El promedio de las edades es: " + str(ca.calcularPromEdades()))
    elif opcion == 5:
        ca.filtroFech(fecha)
    elif opcion == 6:
        pass
    else: 
        print("Esa opcion no esta en el menu, intentelo de nuevo!")      
                
                
                
            
            